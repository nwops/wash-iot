#!/usr/bin/env ruby
require 'json'
require 'logger'

class String
  def colorize(color_code)
    "\e[#{color_code}m#{self}\e[0m"
  end

  def red
    colorize(31)
  end

  def green
    colorize(32)
  end

  def fatal
    red
  end

  def yellow
    colorize(33)
  end
end

module IotUtils
  METHODS = ["list"]
  DEFAULT_TTL = 60 * 5

  def profile
    ENV['AWS_DEFAULT_PROFILE'] || 'default'
  end

  def logger
    @logger ||= begin
      l = Logger.new File.new('/tmp/wash_iot.log', 'a')
      l.formatter = proc do |severity, datetime, progname, msg|
        "[#{severity}]: #{msg}\n"
      end
      l
    end
  end

  def convert_to_wash(things)
      things['things'].map do |thing| 
          
          {
              name: thing['thingName'],
              methods: ['list'],
              cache_ttls: {
                list: 60 * 5
              },
              attributes: { 
                  mtime:  Time.now.to_i,
                  meta: thing['attributes']
              },
              state: thing.to_json
          }
      end
  end

  def array_to_hash(thing)
    if thing.is_a?(Array)
      thing.map.with_index do |value, index| 
        { index.to_s => value }
      end.reduce({}) { | acc, value| acc.merge(value) }
    else
      thing
    end
  end
 
  def convert_entry_to_wash(entry_name, thing)
    logger.debug("converting entry to wash #{entry_name}")
    thing_data = array_to_hash(thing)
    wash_data = thing_data.map do |name, value| 
        method = [String, Integer, Float, Fixnum, Numeric, NilClass].include?(value.class) ? ['read'] : ['list']
        value_data = method.eql?('read') ? value.to_s : value.to_json
        {
            name: name,
            methods: method,
            cache_ttls: {
              list: DEFAULT_TTL
            },
            attributes: { 
                mtime: Time.now.to_i,
                size: value_data.length,
                meta: {}
            },
            state: value_data
        }
    end 
  end

  def shadow_data(name)
    json = run_command("aws iot-data get-thing-shadow --thing-name=#{name} --profile=#{profile} /dev/stdout")
    JSON.parse(json)
  end

  def describe_thing(name, state = nil)
      json = valid_state?(state) ? state : run_command("aws iot describe-thing --profile=#{profile} --thing-name=#{name}")
      data = JSON.parse(json)
      shadow = shadow_data(name) unless data.has_key?('shadow')
      data['shadow'] = shadow
      data
  end

  def run_command(command)
    logger.debug("running: #{command}".fatal)
    `#{command}`
  end

  def list_things(state = nil )
      out = valid_state?(state) ? state : run_command("aws iot list-things --profile=#{profile}")
      things = JSON.parse(out)
      convert_to_wash(things)
  end

  def convert_thing(name, state)
    thing = describe_thing(name)
    convert_entry_to_wash(name, thing)
  end

  def valid_state?(state)
    (!state.nil? && !state.empty?)
  end

  # @args [Array] - an array of command line arguments
  def main(args)
    # <plugin_script> <method> <path> <state> <args...>
    method, path, state, margs = args
    logger.debug("Called with #{method} #{path} <state>")

    data = case method
    when 'init'
      { name: "iot",  methods: METHODS }.to_json
    when 'list'
      name = File.basename(path) 
      _, base, thing_name, sub_path = path.split('/')

      if path.count('/') < 2
        list_things(state).to_json 
      elsif path.count('/') == 2
        thing_data = convert_thing(thing_name, state)
        thing_data.to_json
      elsif name.eql?('shadow')
        entry = begin
          valid_state?(state) ? JSON.parse(state) : shadow_data(thing_name)
        rescue JSON::ParserError => e
          logger.debug(e.message)
          {}
        end
        convert_entry_to_wash(name, entry).to_json
      else
        entry = valid_state?(state) ? JSON.parse(state) : {}
        convert_entry_to_wash(name, entry).to_json
      end
    when 'read'
      logger.debug("reading #{path}")
      state
    else
      nil
    end
    data
  end
end

if ARGV.count > 0
  include IotUtils
  puts main(ARGV)
end
