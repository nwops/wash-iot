#!/usr/bin/env ruby 
require 'json'
require 'logger'
require 'pry'
require 'rb-readline'

PLUGIN = File.join(__dir__, 'iot.rb')
REQUIRED_KEYS = ['name', 'methods', 'attributes', 'state']
def logger
  @logger ||= Logger.new(STDERR)
end

# go through each method and build table then show result
# for each method that is not supported, along with the expect result schema

# Dir.glob('./test/**/run').find_all {|p| File.file?(p) }.each do |path|
#   puts "running #{path}"
#   puts `#{path} #{script}`
# end

def init_methods(entry = nil, base_path = '/')
  entry ||= JSON.parse(`ruby #{PLUGIN} init`)
  path = File.join(base_path, entry['name'])
  [path, entry['methods']]
end

def list_methods(data)

end

def run_methods(script, methods, path, state, args)
  methods.each_with_object({}) do | method, acc| 
    logger.info "Running: #{script} #{method} #{path} ..."
    command = "#{script} #{method} #{path} '#{state}' #{args.join(' ')}"
    acc["#{method}"] = `ruby #{command}` 
  end
end

def validate_entry(entry)
  invalid = entry.keys - REQUIRED_KEYS
  logger.warn("Entry does not contain the following keys #{invalid.join(' ')}") if invalid.count.positive?
end

def validate_metadata(entry)

end

def validate_read(entry)

end

def get_state(entry = nil)
  return nil unless entry
  JSON.parse(entry['state'])
end

def validate_list(entry = nil, base_path = '/')
  path, methods = init_methods(entry, entry['name'])
  args = []
  # this will not work if the state in the entry is nil
  state = get_state(entry) || File.read(File.join('./test/list/list'))
  data = run_methods(PLUGIN, methods, path, state, args).each do | method, result|
    result_data = JSON.parse(result)
    logger.warn("List method does not return an array") unless result_data.is_a?(Array)
    logger.warn("List method does not return an array of hashes") unless result_data.first.is_a?(Hash)
    result_data.each do |entry|
      validate_entry(entry)
      validate_list(entry)
    end
  end
end

validate_init
validate_list




