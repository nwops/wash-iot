#!/usr/bin/env ruby
#require 'rb-readline'
require 'minitest/spec'
require 'minitest'
require 'logger'
require "minitest/autorun"

require 'json'
require 'pry'

REQUIRED_KEYS = ['name', 'methods', 'attributes', 'state']
PLUGIN      = File.expand_path(ARGV[0])
BASE_PATH   = ARGV[1]

if PLUGIN.nil? or PLUGIN.empty? or !File.exist?(PLUGIN)
    puts "Missing plugin path"
    puts "#{__FILE__} <path_to_plugin_file> <base_path>" 
    exit 1
end

if BASE_PATH.nil? or BASE_PATH.empty?
    puts "Missing base path"
    puts "#{__FILE__} <path_to_plugin_file> <base_path>" 
    exit 1
end

class PluginTest < Minitest::Test
  extend Minitest::Spec::DSL

  let(:base_path) do
    BASE_PATH
  end

  def main_list
    @main_list ||= call_script(['list', base_path])
  end

  def test_init_methods
    path, methods = init_methods
    assert(methods.count > 0, "At least one method must exist in init call" )
  end

  def test_init_methods_for_list
    path, methods = init_methods
    assert(methods.find {|m| m.eql?('list')}, "The list method does not exist" )
  end

  def test_init_path
    path, methods = init_methods
    assert(!path.nil?, "A plugin path must exist in init call" )
  end

  def test_init_path_value
    path, methods = init_methods
    assert(BASE_PATH.eql?(path), "A plugin path must exist in init call and be the same" )
  end

  def test_base_list
    assert(valid_list?(main_list), "There was an invalid entry in the list")
  end

  def test_entry_list
    name = main_list[10]['name']
    data = call_script(['list', "#{base_path}/#{name}"])
    assert(valid_list?(data), "There was an invalid entry in the list")
  end

  def test_entry_list_shadow
    name = main_list[10]['name']
    data = call_script(['list', "#{base_path}/#{name}/shadow"])
    assert(valid_list?(data), "There was an invalid entry in the list")
  end

  def test_entry_list_attributes
    name = main_list[10]['name']
    data = call_script(['list', "#{base_path}/#{name}/attributes"])
    assert(valid_list?(data), "There was an invalid entry in the list")
  end

  private

  def init_methods(entry = nil, base_path = '/')
    entry ||= JSON.parse(`ruby #{PLUGIN} init`)
    path = File.join(base_path, entry['name'])
    [path, entry['methods']]
  end

  def logger
    @logger ||=  begin 
      l = Logger.new(STDERR)
      l.formatter = proc do |severity, datetime, progname, msg|
        "\n[#{severity}]: #{msg}\n"
      end
      l
    end
  end

  def call_script(args)
    logger.info("calling: #{PLUGIN} #{args.join(' ')}")
    JSON.parse(`#{PLUGIN} #{args.join(' ')}`)
  end

  def valid_entry?(entry)
    invalid = entry.keys - REQUIRED_KEYS
    invalid.count.zero?
  end

  def valid_list?(entries = nil, base_path = '/')
    items = entries.find do |entry|
      !valid_entry?(entry)
    end
    items.nil? || items.count.zero?
  end

end

