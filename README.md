# AWS IOT plugin for Puppet WASH

A wash plugin for working with AWS IoT items.

- enumerates attributes and shadow data

## This is a WIP

This is just a example / test script to enumerate all of my personal IoT.

This script is non optimized.  Pull requests welcomed

## Requirements

- Wash
- AWS cli
- Ruby

## Setup

Create a wash settings file at ~/.puppetlabs/wash/wash.yaml or similar

```
external-plugins:
  - script: '<path_goes_here>/iot/iot.rb'
```

## How to use

You can call this script using wash or directly for testing.

You must first set a AWS [profile](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html).

Then export this profile so the plugin knows which profile to use.
`export AWS_DEFAULT_PROFILE=user1`

The iot plugin will use the the default profile if the variable is undefined. 
### Directly

`./iot.rb list /iot`
`./iot.rb list /iot/<thing_name>`
`./iot.rb list /iot/<thing_name>/shadow`

### Via Wash

1. Start up the wash server.
2. cd iot/<thing_name>

Use the list/ls command to list the things, attributes and shadow data.

Use cat, more and other ways of reading data. 

## Debugging

The IoT script will send log output to /tmp/wash_iot.log.
You can tail this log at any time. I would recommend manual deletion as
the logs can fill up quickly.

## Examples

```
$ wash
Welcome to Wash!
  Wash includes several built-in commands: wclear, wexec, find, list, meta, tail. Try 'help'.
  Commands run with wash can be seen via 'whistory', and logs for those commands with 'whistory <id>'.
ls
aws        docker     iot        kubernetes
 ~/Library/Caches/wash/mnt506607513  cd iot
 ~/Library/Caches/wash/mnt506607513/iot  ls
0101c6    2439d6    275846    28518a    4c772c    607ede    8a50f6    Testy     c64016    f370cc
016a4b    254e99    275892    2851f8    597hqdur  6081e1    bigrig1   c64017    f986bf
221bf2    275843    2758c6    3ec842    5fa23c    8a4f28    f2439d6   

```

Shadow data

```
 ~/Library/Caches/wash/mnt506607513/iot  cd 597hqdur 
 ~/Library/Caches/wash/mnt506607513/iot/597hqdur  ls
attributes      defaultClientId shadow          thingArn        thingId         thingName       thingTypeName   version
 ~/Library/Caches/wash/mnt506607513/iot/597hqdur  more thingName 
"597hqdur"
 ~/Library/Caches/wash/mnt506607513/iot/597hqdur  cd shadow 
 ~/Library/Caches/wash/mnt506607513/iot/597hqdur/shadow  ls
metadata  state     timestamp version
 ~/Library/Caches/wash/mnt506607513/iot/597hqdur/shadow  cd state 
 ~/Library/Caches/wash/mnt506607513/iot/597hqdur/shadow/state  ls
reported
 ~/Library/Caches/wash/mnt506607513/iot/597hqdur/shadow/state  cd reported 
 ~/Library/Caches/wash/mnt506607513/iot/597hqdur/shadow/state/reported  ls
ambientTemp       cpuTemp           hourlyCost        lastUpdated       minerName         power
apiVersion        earningsCostRatio hourlyEarnings    make              miningSiteId      status
cliVersion        gpu_type          ipAddress         minerAlgo         model             uptime
computeUnits      hashrate          kwhCost           minerCurrency     name

 ~/Library/Caches/wash/mnt506607513/iot/597hqdur/shadow/state/reported  ls
ambientTemp       cpuTemp           hourlyCost        lastUpdated       minerName         power
apiVersion        earningsCostRatio hourlyEarnings    make              miningSiteId      status
cliVersion        gpu_type          ipAddress         minerAlgo         model             uptime
computeUnits      hashrate          kwhCost           minerCurrency     name
 ~/Library/Caches/wash/mnt506607513/iot/597hqdur/shadow/state/reported  more power 
888
 ~/Library/Caches/wash/mnt506607513/iot/597hqdur/shadow/state/reported  more uptime 
4.38
 ~/Library/Caches/wash/mnt506607513/iot/597hqdur/shadow/state/reported  more ambientTemp 
31
 ~/Library/Caches/wash/mnt506607513/iot/597hqdur/shadow/state/reported  more hashrate 
251671000
 ~/Library/Caches/wash/mnt506607513/iot/597hqdur/shadow/state/reported  more gpu_type 
"nvidia"
```

Tree on compute units

```
 ~/Library/Caches/wash/mnt506607513/iot/597hqdur/shadow/state/reported/computeUnits  tree
.
├── 0
│   ├── bios
│   ├── cor
│   ├── dpm
│   ├── earningsCostRatio
│   ├── fanSpeed
│   ├── gpuId
│   ├── hashrate
│   ├── hourlyCost
│   ├── hourlyEarnings
│   ├── make
│   ├── maxFan
│   ├── maxPower
│   ├── maxTemp
│   ├── mem
│   ├── model
│   ├── name
│   ├── ocProfile
│   ├── pciLoc
│   ├── power
│   ├── pstate
│   ├── status
│   ├── subType
│   ├── syspath
│   ├── temperature
│   ├── type
│   ├── utilization
│   ├── uuid
│   ├── vddci
│   ├── vendor
│   └── vlt

```